package drmdb

import (
	"fmt"

	"gitlab.freedesktop.org/emersion/drmdb/drmtree"
	"gitlab.freedesktop.org/emersion/go-drm"
)

func mergeSpecRange(a, b *drmtree.PropertySpecRange) *drmtree.PropertySpecRange {
	result := *a
	if b.Min < result.Min {
		result.Min = b.Min
	}
	if b.Max > result.Max {
		result.Max = b.Max
	}
	return &result
}

func mergeSpecSignedRange(a, b *drmtree.PropertySpecSignedRange) *drmtree.PropertySpecSignedRange {
	result := *a
	if b.Min < result.Min {
		result.Min = b.Min
	}
	if b.Max > result.Max {
		result.Max = b.Max
	}
	return &result
}

func mergeSpecEnum(a, b drmtree.PropertySpecEnum) drmtree.PropertySpecEnum {
	l := a
	for _, e1 := range b {
		found := false
		for _, e2 := range a {
			if e1.Name == e2.Name {
				found = true
				break
			}
		}
		if !found {
			l = append(l, e1)
		}
	}
	return l
}

func mergeSpec(a, b interface{}) (interface{}, error) {
	switch a := a.(type) {
	case *drmtree.PropertySpecRange:
		return mergeSpecRange(a, b.(*drmtree.PropertySpecRange)), nil
	case drmtree.PropertySpecEnum:
		return mergeSpecEnum(a, b.(drmtree.PropertySpecEnum)), nil
	case drm.ObjectType:
		if a != b {
			return nil, fmt.Errorf("mismatched object types: %v and %v", a, b)
		}
		return a, nil
	case *drmtree.PropertySpecSignedRange:
		return mergeSpecSignedRange(a, b.(*drmtree.PropertySpecSignedRange)), nil
	default:
		return nil, fmt.Errorf("unsupported spec type: %T", a)
	}
}
