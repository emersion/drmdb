package drmdb

import (
	"fmt"
	"html/template"
	"io"
	"strings"

	"github.com/labstack/echo/v4"
	"gitlab.freedesktop.org/emersion/go-drm"
)

type tmpl struct {
	t *template.Template
}

var _ = fmt.Printf

func (t *tmpl) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.t.ExecuteTemplate(w, name, data)
}

func loadTemplates() (*tmpl, error) {
	t, err := template.New("drmdb").Funcs(template.FuncMap{
		"join": strings.Join,
		"pct": func(num, tot int) float32 {
			return 100 * float32(num) / float32(tot)
		},
		"pctColor": func(pct float32) template.CSS {
			f := float32(pct) / 100
			hue := int(f * 120)
			return template.CSS(fmt.Sprintf("hsl(%d, 100%%, 30%%)", hue))
		},
		"drmFormatHex": func(f drm.Format) string {
			return fmt.Sprintf("0x%X", uint32(f))
		},
		"drmModifierHex": func(mod drm.Modifier) string {
			return fmt.Sprintf("0x%X", uint64(mod))
		},
		"bits": func(mask uint32) []int {
			var l []int
			for i := 0; i < 32; i++ {
				if mask&(uint32(1)<<uint(i)) != 0 {
					l = append(l, i)
				}
			}
			return l
		},
	}).ParseGlob(PublicDir + "/*.html")

	return &tmpl{t}, err
}
