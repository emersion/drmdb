package drmdoc

//go:generate go run generate.go drivers/gpu/drm/drm_connector.c "standard connector properties"
//go:generate go run generate.go drivers/gpu/drm/drm_connector.c "HDMI connector properties"
//go:generate go run generate.go drivers/gpu/drm/drm_connector.c "Analog TV Connector Properties"
//go:generate go run generate.go drivers/gpu/drm/drm_crtc.c "standard CRTC properties"
//go:generate go run generate.go drivers/gpu/drm/drm_plane.c "standard plane properties"
//go:generate go run generate.go drivers/gpu/drm/drm_blend.c "overview" "plane blending properties"
//go:generate go run generate.go drivers/gpu/drm/drm_color_mgmt.c "overview" "color management properties"
//go:generate go run generate.go drivers/gpu/drm/drm_atomic_uapi.c "explicit fencing properties"
//go:generate go run generate.go drivers/gpu/drm/drm_connector.c "Variable refresh properties"

import (
	"gitlab.freedesktop.org/emersion/go-drm"
)

func Prop(obj drm.ObjectType, name string) string {
	switch obj {
	case drm.ObjectConnector:
		if doc, ok := standardConnectorProperties[name]; ok {
			return doc
		}
		if doc, ok := hdmiConnectorProperties[name]; ok {
			return doc
		}
	case drm.ObjectCRTC:
		if doc, ok := standardCRTCProperties[name]; ok {
			return doc
		}
	case drm.ObjectPlane:
		if doc, ok := standardPlaneProperties[name]; ok {
			return doc
		}
		if doc, ok := planeBlendingProperties[name]; ok {
			return doc
		}
	}
	// The following docs contain mixed plane, connector and CRTC props
	if doc, ok := explicitFencingProperties[name]; ok {
		return doc
	}
	if doc, ok := variableRefreshProperties[name]; ok {
		return doc
	}
	if doc, ok := colorManagementProperties[name]; ok {
		return doc
	}
	return ""
}
