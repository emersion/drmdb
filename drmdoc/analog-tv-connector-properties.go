// Code generated by running "go generate". DO NOT EDIT.

package drmdoc

var analogTVConnectorProperties = map[string]string{
	"TV Mode": "Indicates the TV Mode used on an analog TV connector. The value\nof this property can be one of the following:\n\nNTSC:\n\tTV Mode is CCIR System M (aka 525-lines) together with\n\tthe NTSC Color Encoding.\n\nNTSC-443:\n\n\tTV Mode is CCIR System M (aka 525-lines) together with\n\tthe NTSC Color Encoding, but with a color subcarrier\n\tfrequency of 4.43MHz\n\nNTSC-J:\n\n\tTV Mode is CCIR System M (aka 525-lines) together with\n\tthe NTSC Color Encoding, but with a black level equal to\n\tthe blanking level.\n\nPAL:\n\n\tTV Mode is CCIR System B (aka 625-lines) together with\n\tthe PAL Color Encoding.\n\nPAL-M:\n\n\tTV Mode is CCIR System M (aka 525-lines) together with\n\tthe PAL Color Encoding.\n\nPAL-N:\n\n\tTV Mode is CCIR System N together with the PAL Color\n\tEncoding, a color subcarrier frequency of 3.58MHz, the\n\tSECAM color space, and narrower channels than other PAL\n\tvariants.\n\nSECAM:\n\n\tTV Mode is CCIR System B (aka 625-lines) together with\n\tthe SECAM Color Encoding.\n\nMono:\n\n\tUse timings appropriate to the DRM mode, including\n\tequalizing pulses for a 525-line or 625-line mode,\n\twith no pedestal or color encoding.\n\nDrivers can set up this property by calling\ndrm_mode_create_tv_properties().",
}
