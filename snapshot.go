package drmdb

import (
	"archive/tar"
	"compress/gzip"
	_ "embed"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"

	"gitlab.freedesktop.org/emersion/drmdb/database"
)

//go:embed LICENSE
var license string

func writeSnapshotFile(tw *tar.Writer, p string, fi os.FileInfo) error {
	f, err := os.Open(p)
	if err != nil {
		return err
	}
	defer f.Close()

	if fi == nil {
		fi, err = f.Stat()
		if err != nil {
			return err
		}
	}

	h, err := tar.FileInfoHeader(fi, "")
	if err != nil {
		return err
	}

	if err := tw.WriteHeader(h); err != nil {
		return err
	}

	_, err = io.Copy(tw, f)
	return err
}

func writeLicenseFile(tw *tar.Writer) error {
	h := tar.Header{
		Name: "LICENSE",
		Mode: 0644,
		Size: int64(len(license)),
	}
	if err := tw.WriteHeader(&h); err != nil {
		return err
	}

	_, err := io.WriteString(tw, license)
	return err
}

func writeSnapshot(w io.Writer) error {
	gw := gzip.NewWriter(w)
	defer gw.Close()

	tw := tar.NewWriter(gw)
	defer tw.Close()

	if err := writeLicenseFile(tw); err != nil {
		return err
	}

	files, err := ioutil.ReadDir(database.Dir)
	if err != nil {
		return err
	}

	for _, fi := range files {
		p := filepath.Join(database.Dir, fi.Name())
		if err := writeSnapshotFile(tw, p, fi); err != nil {
			return err
		}
	}

	return nil
}
