GO ?= go
GOFLAGS ?=
DESTDIR ?=
PREFIX ?= /usr/local
BINDIR ?= bin
DATADIR ?= share
LOCALSTATEDIR ?= /var

goldflags = \
	-X 'gitlab.freedesktop.org/emersion/drmdb/database.Dir=$(LOCALSTATEDIR)/lib/drmdb/db' \
	-X 'gitlab.freedesktop.org/emersion/drmdb.PublicDir=$(PREFIX)/$(DATADIR)/drmdb/public'

.PHONY: drmdb install

all: drmdb

drmdb:
	$(GO) build $(GOFLAGS) -ldflags="$(goldflags)" ./cmd/drmdb

install:
	mkdir -p $(DESTDIR)$(PREFIX)/$(BINDIR)
	mkdir -p $(DESTDIR)$(PREFIX)/$(DATADIR)/drmdb
	mkdir -p $(DESTDIR)$(LOCALSTATEDIR)/lib/drmdb
	cp -f drmdb $(DESTDIR)$(PREFIX)/$(BINDIR)
	cp -rf public/ $(DESTDIR)$(PREFIX)/$(DATADIR)/drmdb/public/

clean:
	rm -f drmdb
