module gitlab.freedesktop.org/emersion/drmdb

go 1.19

require (
	github.com/labstack/echo/v4 v4.13.3
	github.com/labstack/gommon v0.4.2
	github.com/mcuadros/go-version v0.0.0-20190830083331-035f6764e8d2
	gitlab.freedesktop.org/emersion/go-drm v0.0.0-20241124230048-81806c418074
	gitlab.freedesktop.org/emersion/go-hwids v0.0.0-20250306083743-bbc51d4e57af
)

require (
	github.com/dave/jennifer v1.7.1 // indirect
	github.com/mattn/go-colorable v0.1.14 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	github.com/rjeczalik/pkgconfig v0.0.0-20190903131546-94d388dab445 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.2.2 // indirect
	golang.org/x/crypto v0.33.0 // indirect
	golang.org/x/net v0.35.0 // indirect
	golang.org/x/sys v0.30.0 // indirect
	golang.org/x/text v0.22.0 // indirect
	golang.org/x/time v0.10.0 // indirect
	modernc.org/cc/v4 v4.23.1 // indirect
	modernc.org/mathutil v1.6.0 // indirect
	modernc.org/opt v0.1.3 // indirect
	modernc.org/sortutil v1.2.0 // indirect
	modernc.org/strutil v1.2.0 // indirect
	modernc.org/token v1.1.0 // indirect
)
