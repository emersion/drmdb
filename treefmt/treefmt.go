// Package treefmt provides tree pretty-printing helpers.
package treefmt

// Formatter pretty-prints tree structures.
type Formatter interface {
	// NewChild creates a new child tree.
	NewChild() Formatter
	// Printf creates a new child leaf.
	Printf(format string, v ...interface{})
}
