package treefmt

import (
	"fmt"
)

type Memory struct {
	Text     string
	Children []Memory
}

type MemoryFormatter struct {
	m *Memory
}

// NewMemoryFormatter pretty-prints a tree in an in-memory data structure.
func NewMemoryFormatter() *MemoryFormatter {
	return &MemoryFormatter{m: &Memory{}}
}

func (mf *MemoryFormatter) NewChild() Formatter {
	if len(mf.m.Children) == 0 {
		panic("treefmt: NewChild called before Printf")
	}
	m := &mf.m.Children[len(mf.m.Children)-1]
	return &MemoryFormatter{m: m}
}

func (mf *MemoryFormatter) Printf(format string, v ...interface{}) {
	t := fmt.Sprintf(format, v...)
	mf.m.Children = append(mf.m.Children, Memory{Text: t})
}

func (mf *MemoryFormatter) Tree() []Memory {
	return mf.m.Children
}
