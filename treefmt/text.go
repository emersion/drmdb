package treefmt

import (
	"fmt"
	"io"
	"strings"
)

type textFormatter struct {
	w      io.Writer
	indent int
}

// NewTextFormatter pretty-prints a tree in a text format to w.
func NewTextFormatter(w io.Writer) Formatter {
	return &textFormatter{w: w}
}

func (tp *textFormatter) NewChild() Formatter {
	return &textFormatter{w: tp.w, indent: tp.indent + 1}
}

func (tp *textFormatter) Printf(format string, v ...interface{}) {
	fmt.Fprintf(tp.w, strings.Repeat("  ", tp.indent)+format+"\n", v...)
}
