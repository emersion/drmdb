# [drmdb]

A Direct Rendering Manager database.

## Building

When developing:

    go run ./cmd/drmdb

For a production build:

    make
    make install

## License

MIT

[drmdb]: https://drmdb.emersion.fr
