package database

import (
	"gitlab.freedesktop.org/emersion/drmdb/drmtree"
)

const cacheSize = 1000

type cacheEntry struct {
	key  string
	node *drmtree.Node
}

type DB struct {
	cache   []cacheEntry   // ring buffer
	indexes map[string]int // key → cache index
	cur     int
}

func Open() (*DB, error) {
	if err := initDB(); err != nil {
		return nil, err
	}
	return &DB{
		cache:   make([]cacheEntry, cacheSize),
		indexes: make(map[string]int, cacheSize),
	}, nil
}

func (db *DB) storeCache(k string, n *drmtree.Node) {
	prev := db.cache[db.cur]
	delete(db.indexes, prev.key)
	db.cache[db.cur] = cacheEntry{key: k, node: n}
	db.indexes[k] = db.cur
	db.cur = (db.cur + 1) % len(db.cache)
}

func (db *DB) loadCache(k string) *drmtree.Node {
	i, ok := db.indexes[k]
	if !ok {
		return nil
	}
	return db.cache[i].node
}

func (db *DB) Store(n *drmtree.Node) (string, error) {
	k, err := store(n)
	if err == nil {
		db.storeCache(k, n)
	}
	return k, err
}

func (db *DB) Load(k string) (*drmtree.Node, error) {
	if n := db.loadCache(k); n != nil {
		return n, nil
	}
	n, err := load(k)
	if err == nil {
		db.storeCache(k, n)
	}
	return n, err
}

func (db *DB) Walk(fn func(k string, n *drmtree.Node) error) error {
	return walk(func(k string) error {
		n, err := db.Load(k)
		if err != nil {
			return err
		}
		return fn(k, n)
	})
}

func (db *DB) Close() error {
	db.cache = nil
	db.indexes = nil
	return nil
}
